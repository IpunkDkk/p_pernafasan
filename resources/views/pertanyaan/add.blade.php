@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('pertanyaan.add')}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <label for="gejala" class="form-label">Pertanyaan</label>
            <input type="text" class="form-control" id="pertanyaan" name="pertanyaan">
        </div>
        <button type="submit" class="btn btn-primary">Tambah Pertanyaan</button>
    </form>
</div>
@endsection
