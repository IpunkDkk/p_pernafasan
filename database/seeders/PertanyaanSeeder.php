<?php

namespace Database\Seeders;

use App\Models\pertanyaan;
use Illuminate\Database\Seeder;

class PertanyaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pertanyaan = new pertanyaan();
        $pertanyaan->create([
            'pertanyaan' => 'apakah anda batuk?'
        ]);
    }
}
