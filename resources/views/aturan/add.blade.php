@extends('template.content')
@section('content')
<div class="container p-5">
    <form action="{{route('aturan.add')}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <select name="pertanyaan" class="form-select form-select" aria-label=".form-select-sm example">
                <option selected>pertanyaan</option>
                @foreach ($pertanyaan as $item)
                  <option value="{{ $item->pertanyaan }}">{{ $item->pertanyaan }}
                </option>
                @endforeach
            </select>   
        </div>
        <div class="mb-3">
            <select name="gejala" class="form-select form-select" aria-label=".form-select-sm example">
                <option selected>gejala</option>
                @foreach ($gejala as $item)
                  <option value="{{ $item->gejala }}">{{ $item->gejala }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Tambah Gejala</button>
    </form>
</div>
@endsection
