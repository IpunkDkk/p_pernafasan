<?php

namespace App\Http\Controllers;

use App\Models\aturan;
use App\Models\gejala;
use App\Models\pertanyaan;
use Illuminate\Http\Request;

class AturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = aturan::all();
        // $pertanyaan = pertanyaan::all()->where('id','=',$data->id_pertanyaan);

        // $pertanyaan = pertanyaan::all();
        // var_dump($data);
        // dd($data);
        return view('aturan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pertanyaan = pertanyaan::all();
        $gejala = gejala::all();
        return view('aturan.add', compact(['pertanyaan', 'gejala']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_pertanyaan = pertanyaan::all()->where('pertanyaan','=',$request->pertanyaan);
        foreach ($id_pertanyaan as $key) {
            $id_pertanyaan = $key['id'];
        }
        $id_gejala = gejala::all()->where('gejala','=',$request->gejala);
        foreach ($id_gejala as $key) {
            $id_gejala = $key['id'];
        }
        $data = $request->validate([
            'pertanyaan'=>'required',
            'gejala'=>'required',
        ]);
        $data['id_pertanyaan'] = $id_pertanyaan;
        $data['id_gejala'] = $id_gejala;
        aturan::create($data);
        return redirect()->route('aturan.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\aturan  $aturan
     * @return \Illuminate\Http\Response
     */
    public function show(aturan $aturan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\aturan  $aturan
     * @return \Illuminate\Http\Response
     */
    public function edit(aturan $aturan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\aturan  $aturan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, aturan $aturan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\aturan  $aturan
     * @return \Illuminate\Http\Response
     */
    public function destroy(aturan $aturan)
    {
        //
    }
}
