<?php

namespace Database\Seeders;

use App\Models\gejala;
use Illuminate\Database\Seeder;

class GejalaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gejala = new gejala();
        $gejala->create([
            'gejala'=>'TBC'
        ]);
    }
}
