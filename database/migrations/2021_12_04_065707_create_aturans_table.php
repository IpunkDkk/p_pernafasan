<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAturansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aturans', function (Blueprint $table) {
            $table->id();
            $table->integer('j1')->nullable();
            $table->integer('j2')->nullable();
            $table->integer('j3')->nullable();
            $table->integer('j4')->nullable();
            $table->integer('j5')->nullable();
            $table->integer('j6')->nullable();
            $table->integer('j7')->nullable();
            $table->foreignId('id_gejala')->constrained()->onUpdate('cascade')->onDelete('cascade');

            // $table->string('pertanyaan');
            // $table->string('gejala');
            // $table->foreignId('id_pertanyaan')->constrained()->onUpdate('cascade')->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aturans');
    }
}
