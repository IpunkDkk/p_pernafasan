<?php

use App\Http\Controllers\AturanController;
use App\Http\Controllers\GejalaController;
use App\Http\Controllers\KonsultasiController;
use App\Http\Controllers\PertanyaanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::prefix('konsultasi')->group(function(){
    Route::get('/',[KonsultasiController::class,'index'])->name('konsultasi.index');
    Route::get('add',[KonsultasiController::class, 'create'])->name('konsultasi.add');
    Route::post('add',[KonsultasiController::class, 'store'])->name('konsultasi.add');
    Route::delete('delete/{id}',[KonsultasiController::class, 'destroy'])->name('konsultasi.delete');
});

Route::prefix('gejala')->group(function(){
    Route::get('/',[GejalaController::class,'index'])->name('gejala.index');
    Route::get('add',[GejalaController::class, 'create'])->name('gejala.add');
    Route::post('add',[GejalaController::class, 'store'])->name('gejala.add');
    Route::delete('delete/{id}',[GejalaController::class, 'destroy'])->name('gejala.delete');
});

Route::prefix('pertanyaan')->group(function(){
    Route::get('/',[PertanyaanController::class,'index'])->name('pertanyaan.index');
    Route::get('add',[PertanyaanController::class, 'create'])->name('pertanyaan.add');
    Route::post('add',[PertanyaanController::class, 'store'])->name('pertanyaan.add');
    Route::delete('delete/{id}',[PertanyaanController::class, 'destroy'])->name('pertanyaan.delete');
});

Route::prefix('aturan')->group(function(){
    Route::get('/',[AturanController::class,'index'])->name('aturan.index');
    Route::get('add',[AturanController::class, 'create'])->name('aturan.add');
    Route::post('add',[AturanController::class, 'store'])->name('aturan.add');
    Route::delete('delete/{id}',[AturanController::class, 'destroy'])->name('aturan.delete');
});