<?php

namespace App\Http\Controllers;

use App\Models\gejala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GejalaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = gejala::all();
        return view('gejala.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gejala.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'gejala' => 'required',
        ]);
        gejala::create($data);
        return Redirect()->route('gejala.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gejala  $gejala
     * @return \Illuminate\Http\Response
     */
    public function show(gejala $gejala)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gejala  $gejala
     * @return \Illuminate\Http\Response
     */
    public function edit(gejala $gejala)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gejala  $gejala
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gejala $gejala)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gejala  $gejala
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = gejala::find($id);
        $data->delete();
        return Redirect()->route('gejala.index');
    }
}
