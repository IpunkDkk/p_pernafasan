@extends('template.content')
@section('content')
<div class="container">
    <a class="btn btn-primary" role="button" href="{{ route('pertanyaan.add') }}">Tambah Pertanyaan</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">NO</th>
            <th scope="col">ID</th>
            <th scope="col">pertanyaan</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($data as $item)    
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $item->id }}</td>
          <td>{{ $item->pertanyaan }}</td>
          <td>
            <form method="POST" action="{{ route('pertanyaan.delete', $item->id) }}" id="hapus">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger btn-sm" type="submit">Hapus</button>
            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
</div>
@endsection